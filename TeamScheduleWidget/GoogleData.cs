﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamScheduleWidget
{
    public class GoogleData
    {
        List<string> Colums = new List<string>();
        List<string> RowItems = new List<string>();

        public GoogleData() { }

        public void AddItemToColums(string _item){ Colums.Add(_item); }

        public void AddItemToRowItems(string _item) { RowItems.Add(_item); }

        public void ClearColums() { Colums.Clear(); }

        public void ClearRowItems() { RowItems.Clear(); }

        public string GetColumItem(int _index) {
            if (_index < Colums.Count)
            {
                return Colums[_index];
            }
            else
            {
                return "none";
            }
        }

        public string GetRowItem(int _index) {
            if (_index < RowItems.Count)
            {
                return RowItems[_index];
            }
            else
            {
                return "none";
            }
        }

        public int GetColumCount() { return Colums.Count; }

        public int GetRowItemsCount() { return RowItems.Count; }
    }
}
