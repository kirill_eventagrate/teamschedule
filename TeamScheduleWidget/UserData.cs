﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamScheduleWidget
{
    [Serializable]
    public class UserData
    {
        public string Month;
        public int NameIndex;
        public string Name;

        public UserData() { }

        public void SetUserData(string _month, int _nameIndex, string _name)
        {
            Month = _month;
            NameIndex = _nameIndex;
            Name = _name;
        }

        public int GetNameIndex() { return NameIndex; }
        public string GetMonth() { return Month; }
    }
}
