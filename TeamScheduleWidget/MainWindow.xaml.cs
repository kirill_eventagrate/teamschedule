﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Windows.Controls;
using System.Data;
using System.Windows.Data;

namespace TeamScheduleWidget
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/sheets.googleapis.com-dotnet-quickstart.json
        static string[] Scopes = { SheetsService.Scope.SpreadsheetsReadonly };
        static string ApplicationName = "Google Sheets API .NET Quickstart";
        GoogleData googleData = new GoogleData();
        UserData userData = new UserData();

        public MainWindow()
        {
            try
            {
                InitializeComponent();

                LoadUserData();

                GetSheets();
                GetNameList(monthComboBox.Items[monthComboBox.SelectedIndex].ToString());
                try
                {
                    InitGoogleData(googleData);
                    DrawListFotToday();
                    DrawListView(googleData);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        

        void LoadUserData()
        {
            //Properties.Settings.Default.Reset();
            string line = Properties.Settings.Default["userData"].ToString();
            Console.WriteLine(line);
            if (line.Trim() != "")
            {
                dynamic tmpUserData = JObject.Parse(line.Trim());
                Console.WriteLine(tmpUserData);
                Console.WriteLine(tmpUserData.Month.ToObject<string>());
                userData.SetUserData(tmpUserData.Month.ToObject<string>(), tmpUserData.NameIndex.ToObject<int>(), tmpUserData.Name.ToObject<string>());
            }
        }

        public void SaveUserData()
        {
            string jsonStr = JsonConvert.SerializeObject(userData);
            Console.WriteLine("user data: " + jsonStr);
            Properties.Settings.Default["userData"] = jsonStr;
            Properties.Settings.Default.Save();
        }

        public void GetSheets()
        {
            UserCredential credential;

            using (var stream =
                new FileStream("Resourse/credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            // Define request parameters.
            String spreadsheetId = "1LdH2yodOjRuyzzF1WeVcqAco65RNLJrHZsNgw8Mz9vA";
            List<string> ranges = new List<string>();

            

            // True if grid data should be returned.
            // This parameter is ignored if a field mask was set in the request.
            bool includeGridData = false;  // TODO: Update placeholder value.

            SpreadsheetsResource.GetRequest request = service.Spreadsheets.Get(spreadsheetId);
            request.Ranges = ranges;
            request.IncludeGridData = includeGridData;

            Google.Apis.Sheets.v4.Data.Spreadsheet response = request.Execute();
            dynamic stuff = JObject.Parse(JsonConvert.SerializeObject(response));
            
            monthComboBox.Items.Clear();
            int selectIndex = 0;
            int i = 0;
            foreach(var sheet in stuff.sheets) //for(int i = 0; i < stuff.sheets.Count; i++) //
            {
                if (sheet.properties.hidden != true)
                {
                    monthComboBox.Items.Add(sheet.properties.title);
                    Console.WriteLine(sheet.properties.title.ToString() + ":" + userData.GetMonth());
                    if (sheet.properties.title.ToString() == userData.GetMonth())
                    {
                        selectIndex = i;
                    }
                    i++;
                }
            }
            Console.WriteLine(selectIndex);
            monthComboBox.SelectedIndex = selectIndex;
        }
        
        public void GetNameList(string _sheetName)
        {
            UserCredential credential;

            using (var stream =
                new FileStream("Resourse/credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            // Define request parameters.
            String spreadsheetId = "1LdH2yodOjRuyzzF1WeVcqAco65RNLJrHZsNgw8Mz9vA";
            List<string> ranges = new List<string>();
            ranges.Add(_sheetName + "!A1:A40");

            SpreadsheetsResource.ValuesResource.BatchGetRequest.ValueRenderOptionEnum valueRenderOption = (SpreadsheetsResource.ValuesResource.BatchGetRequest.ValueRenderOptionEnum)0;  // TODO: Update placeholder value.
            SpreadsheetsResource.ValuesResource.BatchGetRequest.DateTimeRenderOptionEnum dateTimeRenderOption = (SpreadsheetsResource.ValuesResource.BatchGetRequest.DateTimeRenderOptionEnum)0;  // TODO: Update placeholder value.
            SpreadsheetsResource.ValuesResource.BatchGetRequest request = service.Spreadsheets.Values.BatchGet(spreadsheetId);
            request.Ranges = ranges;
            request.ValueRenderOption = valueRenderOption;
            request.DateTimeRenderOption = dateTimeRenderOption;

            Google.Apis.Sheets.v4.Data.BatchGetValuesResponse response = request.Execute();
            
            dynamic stuff = JObject.Parse(JsonConvert.SerializeObject(response));
            //Console.WriteLine(stuff.valueRanges[0].values[2]);
            
            nameComboBox.Items.Clear();
            foreach (var name in stuff.valueRanges[0].values)
            {
                foreach (var key in name)
                {
                    //ListBoxItem item = new ListBoxItem();
                    //item.Content = key;
                    nameComboBox.Items.Add(key);
                }
            }
            int selectIndex = 1;
            if (userData.GetNameIndex() > 2 && userData.GetNameIndex() - 2 > 0)
            {
                selectIndex = userData.GetNameIndex() - 2;
            }
            nameComboBox.SelectedIndex = selectIndex;
        }

        public void InitGoogleData(GoogleData _googleData)
        {
            UserCredential credential;

            using (var stream =
                new FileStream("Resourse/credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            // Define request parameters.
            String spreadsheetId = "1LdH2yodOjRuyzzF1WeVcqAco65RNLJrHZsNgw8Mz9vA";
            List<string> ranges = new List<string>();
            /*ranges.Add("Nov 18!A1:AF1");
            ranges.Add("Nov 18!A19:AF19");*/
            string Month = userData.GetMonth();
            if (Month == "None")
            {
                Month = monthComboBox.Items[monthComboBox.SelectedIndex].ToString();
            }
            ranges.Add(Month + "!A1:AF1");
            ranges.Add(Month + "!A" + userData.GetNameIndex() + ":AF" + userData.GetNameIndex());


            SpreadsheetsResource.ValuesResource.BatchGetRequest.ValueRenderOptionEnum valueRenderOption = (SpreadsheetsResource.ValuesResource.BatchGetRequest.ValueRenderOptionEnum)0;  // TODO: Update placeholder value.

            SpreadsheetsResource.ValuesResource.BatchGetRequest.DateTimeRenderOptionEnum dateTimeRenderOption = (SpreadsheetsResource.ValuesResource.BatchGetRequest.DateTimeRenderOptionEnum)0;  // TODO: Update placeholder value.

            SpreadsheetsResource.ValuesResource.BatchGetRequest request = service.Spreadsheets.Values.BatchGet(spreadsheetId);
            request.Ranges = ranges;
            request.ValueRenderOption = valueRenderOption;
            request.DateTimeRenderOption = dateTimeRenderOption;


            Google.Apis.Sheets.v4.Data.BatchGetValuesResponse response = request.Execute();


            //Console.WriteLine(JsonConvert.SerializeObject(response));
            dynamic stuff = JObject.Parse(JsonConvert.SerializeObject(response));
            _googleData.ClearColums();
            _googleData.ClearRowItems();
            foreach (var row in stuff.valueRanges[0].values)
            {
                foreach (var coll in row)
                {
                    _googleData.AddItemToColums(coll.ToString());
                }
            }

            foreach (var row in stuff.valueRanges[1].values)
            {
                foreach (var coll in row)
                {
                    _googleData.AddItemToRowItems(coll.ToString());
                }
            }
        }

        public void DrawListFotToday()
        {
            NameLable.Content = userData.Name;
            listBox.Items.Clear();
            string dataStr = DateTime.Now.ToString("dd.M.yyyy");
            //Console.WriteLine("day: " + day);
            for (int i = 0; i < googleData.GetColumCount(); i++)
            {
                if (googleData.GetColumItem(i).Trim() == dataStr.Trim())
                {
                    TextBlock textBlock = new TextBlock();
                    ListBoxItem listBoxItem = new ListBoxItem();
                    listBoxItem.Content = "Today: " + googleData.GetRowItem(i);
                    
                    listBox.Items.Add(listBoxItem);
                    if (googleData.GetColumItem(i + 1) != "none")
                    {
                        listBox.Items.Add(googleData.GetColumItem(i + 1) + ": " + googleData.GetRowItem(i + 1));
                    }
                    if (googleData.GetColumItem(i + 2) != "none")
                    {
                        listBox.Items.Add(googleData.GetColumItem(i + 2) + ": " + googleData.GetRowItem(i + 2));
                    }
                    break;
                }
            }
        }

        public void DrawListView(GoogleData _googleData)
        {
            listView.Items.Clear();
            var gv = new GridView();
            listView.View = gv;
            for (int index = 0; index < _googleData.GetColumCount(); index++)
            {
                gv.Columns.Add(new GridViewColumn
                {
                    Header = _googleData.GetColumItem(index),
                    DisplayMemberBinding = new Binding("[" + index.ToString() + "]")
                });
            }
            List<string> rowItems = new List<string>();
            for (int index = 0; index < _googleData.GetRowItemsCount(); index++)
            {
                rowItems.Add(_googleData.GetRowItem(index));
            }
            listView.Items.Add(rowItems);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            userData.SetUserData(monthComboBox.Items[monthComboBox.SelectedIndex].ToString(), nameComboBox.SelectedIndex + 2, nameComboBox.Items[nameComboBox.SelectedIndex].ToString());
            SaveUserData();
            InitGoogleData(googleData);
            DrawListFotToday();
            DrawListView(googleData);
            Console.WriteLine("Button_Click " + userData.Name);
        }
    }
}
